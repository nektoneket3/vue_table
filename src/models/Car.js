import uniqid from 'uniqid';


export class Car {
  constructor({
    model = '',
    year = '',
    color = '',
    audiosystem = null,
    additional = '',
    id = uniqid()
    } = {}) {
    this.model = model;
    this.year = year;
    this.color = color;
    this.audiosystem = audiosystem;
    this.additional = additional;
    this.id = id;
  }
}
