import Vue from 'vue';
import App from './App.vue';
// components
import vSelect from 'vue-select';
import Select from './components/Select';
// styles
import 'vue-select/dist/vue-select.css';


Vue.component('v-select', vSelect);
Vue.component('Select', Select);

Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
}).$mount('#app');
